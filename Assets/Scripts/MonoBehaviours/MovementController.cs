using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float movementSpeed = 3.0f;
    Vector2 movement = new Vector2();

    Animator animator;

    //readonly string animationState = "AnimationState";
    Rigidbody2D rb2D;

    //enum CharStates
    //{
    //    walkEast = 1,
    //    walkSouth = 2,
    //    walkWest = 3,
    //    walkNorth = 4,
    //    idleSouth = 5,
    //}
         
    void Start()
    {
        animator = this.GetComponent<Animator>();
        rb2D = this.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        UpdateState();
    }

    private void FixedUpdate()
    {
        MoveCharacter();
    }

    private void MoveCharacter()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        movement.Normalize();

        rb2D.velocity = movement * movementSpeed;
    }

    private void UpdateState()
    {
        if (Mathf.Approximately(movement.x, 0) && Mathf.Approximately(movement.y, 0))
        {
            animator.SetBool("isWalking", false);
        }
        else
        {
            animator.SetBool("isWalking", true);
        }

        animator.SetFloat("xDir", movement.x);
        animator.SetFloat("yDir", movement.y);
    }
}
