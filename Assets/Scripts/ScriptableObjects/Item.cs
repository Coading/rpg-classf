using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item")]
public class Item : ScriptableObject
{
    public string objectName;

    public Sprite sprite;

    public int quantity; // 아이템의 수량

    public bool stackable; // false 이면 한 번에 여러 개를 처리 할 수 없다

    public enum ItemType
    {
        COIN,
        HEALTH
    }

    public ItemType itemType;
}
